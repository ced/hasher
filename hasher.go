package main

import (
	"fmt"
	"gitlab.com/ced/hasher/hasher"
	"log"
	"os"
	"path/filepath"
	"sync"
)

// hashResponseHandler performs an action for each file that has been hashed.
func hashResponseHandler(out <-chan hasher.Response, handlerGroup *sync.WaitGroup) {
	for r := range out {
		if r.Err != nil {
			panic(fmt.Errorf("Failed to hash file %s: %v", r.Name, r.Err))
		}

		fmt.Println(r)
	}
	handlerGroup.Done()
}

func main() {
	if len(os.Args) == 1 {
		log.Fatalf("Usage: %s <path to files>, ...")
	}

	var fileNames = make(chan hasher.Request)
	var hashes = make(chan hasher.Response)
	var wg sync.WaitGroup

	// Start hasher
	var h = hasher.NewHasher(hasher.WithIn(fileNames))

	// Read the hashes channel, and perform an operation (print out the hash)
	wg.Add(1)
	go hashResponseHandler(hashes, &wg)

	// Define a function that feeds file names into the Hasher
	hashFiles := hasher.FileToFn(fileNames, hashes)

	for _, path := range os.Args[1:] {
		err := filepath.Walk(path, hashFiles)
		if err != nil {
			log.Fatalf("Failed walk of %s: %v\n", path, err)
		}
	}

	// No more files that we need hashed
	close(fileNames)
	// Wait for the hasher to finish
	h.Wait()

	// No more hashes we need printed out
	close(hashes)
	// Wait for the hash response handler to finish
	wg.Wait()
}
