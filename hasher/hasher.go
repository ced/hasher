package hasher

import (
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"runtime"
	"sync"
)

type HasherOption func(h *Hasher)

// Hasher uses goroutines to provide async file hashing
type Hasher struct {
	hashFn func(string) (string, error)
	In         chan Request
	maxWorkers int
	wg         sync.WaitGroup
}

// Request contains a hash request
type Request struct {
	Name string
	Answer chan Response
}

// Response contains a hash response
type Response struct {
	Name string
	Hash string
	Err  error
}

func (h *Hasher) worker() {
	for r := range h.In {
		aString, err := h.hashFn(r.Name)
		r.Answer <- Response{Name: r.Name, Hash: aString, Err: err}
	}
}

// Hash hashes a file and issues the response
func (h *Hasher) Hash(name string) Response {
	answer := h.HashAsync(name)
	defer close(answer)
	return <-answer
}

// HashAsync issues a Request to hash a file, and returns a channel that the response will be sent on.
func (h *Hasher) HashAsync(name string) chan Response {
	answer := make(chan Response)
	h.In <- Request{Name: name, Answer: answer}
	return answer
}

// HashTo issues a Request to hash a file, with the given channel for the response.
func (h *Hasher) HashTo(name string, to chan Response) {
	h.In <- Request{Name: name, Answer: to}
}

// Start starts a goroutine to perform hashing
func (h *Hasher) Start() {
	for i := 0; i < h.maxWorkers; i++ {
		h.wg.Add(1)
		go func() {
			h.worker()
			h.wg.Done()
		}()
	}
}

// Stop stops the hasher
func (h *Hasher) Stop() {
	close(h.In)
	h.Wait()
}

// Wait waits for the hasher message handler to finish
func (h *Hasher) Wait() {
	h.wg.Wait()
}

// WithIn specifies another channel to use as the input channel
func WithIn(in chan Request) HasherOption {
	return func(h *Hasher) {
		oldIn := h.In
		h.In = in
		close(oldIn)
	}
}

// WithMaxWorkers modifies the Hasher to use the given number of maximum goroutines for hasher workers
func WithMaxWorkers(max int) HasherOption {
	return func(h *Hasher) {
		h.maxWorkers = max
	}
}

// WithHashFn modifies the Hasher to use the given hashing function
func WithHashFn(fn func(string) (string, error)) HasherOption {
	return func(h *Hasher) {
		h.hashFn = fn
	}
}

// NewHasher returns a new Hasher type
func NewHasher(opts ...HasherOption) *Hasher {
	var wg sync.WaitGroup
	h := Hasher{
		hashFn: Sha1File,
		In:         make(chan Request),
		maxWorkers: runtime.NumCPU(),
		wg:         wg,
	}

	// Modify default options
	for _, fn := range opts {
		fn(&h)
	}

	h.Start()

	return &h
}

// Sha1File returns the SHA1 hash of the given file name
func Sha1File(name string) (string, error) {
	fh, err := os.Open(name)
	if err != nil {
		return "", err
	}
	defer func() {
		_ = fh.Close()
	}()

	h := sha1.New()
	_, err = io.Copy(h, fh)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%x", h.Sum(nil)), nil
}