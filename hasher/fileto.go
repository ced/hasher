package hasher

import "os"

// FileToFn returns a function that feeds files found into the given channel.
// Meant to be used with filepath.Walk
func FileToFn(in chan<- Request, out chan Response) (func(string, os.FileInfo, error) error) {
	closure := func(n string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if !info.Mode().IsRegular() {
			return nil
		}

		in <- Request{Name: n, Answer: out}

		return nil
	}

	return closure
}
